angular.module('suki_app.services', ['ionic'])
.factory('Locations', function() {
  // Might use a resource here that returns a JSON array

  // Some fake testing data
  var locations = [
      {
        id: 0,
        title: 'biker jims',
        img: 'https://41.media.tumblr.com/a9ccbf11287b2a297e42dde6783b71fa/tumblr_o2mf1bRnBS1qmjb2ao1_500.jpg',
        distance: '3mi',
        hours: '2pm-4pm',
        drink_deals: 'PBR $1.00',
        food_deals: 'Hotdogs $2.00',
        address: '123 fake st',
        phonenumber: '832388388',
        stars: new Array(5),
        reviews: 'steve: lmao these dogs SUCK'
      }
    ];

  return {
    all: function() {
      return locations;
    },
    remove: function(location) {
      chats.splice(chats.indexOf(location), 1);
    },
    get: function(locationId) {
      for (var i = 0; i < locations.length; i++) {
        if (locations[i].id === parseInt(locationId)) {
          return locations[i];
        }
      }
      return null;
    }
  };
});
