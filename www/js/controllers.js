// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('suki_app.controllers', ['ionic'])

.controller('ListCtrl', function($scope, $http) {
  $http.get('http://localhost:8000/locations').then(function(resp) {
    console.log('Success', resp);
    $scope.locations = resp.data;
    // For JSON responses, resp.data contains the result
  }, function(err) {
    console.error('ERR', err);
    // err.status will contain the status code
  })
})

.controller('LocationDetailCtrl', function($scope, $stateParams, Locations) {
  $scope.location = Locations.get($stateParams.locationId);
})

.controller('MapCtrl', function($scope) {});
