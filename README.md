suki_app
========

Getting Started
---------------

Getting started on Windows:

1. Install Node.js ([Windows x64](https://nodejs.org/dist/v4.3.0/node-v4.3.0-x64.msi))
2. Install git ([Windows x64](https://github.com/git-for-windows/git/releases/download/v2.7.1.windows.2/Git-2.7.1.2-64-bit.exe))
3. Install Atom.io ([Windows](https://github.com/atom/atom/releases/download/v1.5.3/AtomSetup.exe))
3. Open the Command Prompt
4. Run `npm install -g cordova ionic gulp`
5. Run `git clone https://github.com/SShinol/suki_app.git`
6. Run `cd suki_app`
7. Open the project in Atom; it should be in `C:\Users\MyUserName\suki_app`


Developing
----------

The files for the UI are in `www/templates`; you can edit them with Atom.

Open the Command Prompt, and `cd suki_app`. From there, you can:

* `git pull` to pull in new changes from GitHub
* `ionic serve` to run the app in your browser
* Use [other git commands](https://try.github.io/) to commit and push your code to GitHub

The Ionic docs are [here](http://ionicframework.com/docs/).

The Material docs are [here](https://www.google.com/design/spec/), in case
you want to see some design guidelines.
